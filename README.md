# Laravel View Projects

## Screenshoots
![image1](/public/Screenshot-1.png)
![image1](/public/Screenshot-2.png)
![image1](/public/Screenshot-3.png)
![image1](/public/Screenshot-4.png)

## Extends and Section
app.blade.php
```php
@include('includes.header')

@yield('content')

@include('includes.footer')
```
home.blade.php
```php

@extends('layouts.app')

@section('content')
<div class="relative overflow-hidden px-6 pb-6">
    <img src="{{url("/images/wave.svg")}}" class="absolute top-0 left-2/5">
    <div class="container mx-auto relative">
        <div class="flex flex-col md:flex-row items-center pt-32 pb-16 md:pb-0">
            <div class="md:w-1/2 lg:w-1/3 mb-4 sm:mb-16 md:mb-0">
                <h2 class="text-xl font-bold text-secondary-600 uppercase mb-2">Lorem ipsum dolor</h2>
                <h1 class="text-4xl md:text-5xl font-bold text-secondary leading-tight mb-6 md:mb-10">Social Network
                    Management</h1>
                <a href="#"
                    class="bg-primary px-6 md:px-8 py-3 md:py-4 text-lg md:text-xl text-secondary font-bold uppercase rounded hover:bg-primary-400">More
                    info</a>
            </div>
            <div class="mt-16 sm:mt-0 flex-1 flex justify-end">
                <img src="{{url("/images/hero.svg")}}">
            </div>
        </div>
    </div>
</div>
@endsection

```

## Foreach
testimonial.blade.php
```php
@foreach ($images as $image)
@if ($loop->index == 2)
<a href="#">
    <img src="{{url($image)}}"
        class="w-16 h-16 md:w-32 md:h-32 lg:w-48 lg:h-48 mx-2 lg:mx-4 object-cover rounded-full border-2 md:border-4 border-white">
</a>
@else

<a href="#">
    <img src="{{url($image)}}"
        class="w-12 h-12 md:w-24 md:h-24 lg:w-32 lg:h-32 mx-2 lg:mx-4 object-cover rounded-full border-2 md:border-4 border-white">
</a>
@endif

@endforeach
```
