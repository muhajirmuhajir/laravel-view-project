<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
})->name('home');

Route::get('/contact', function () {
    $images = [
        '/images/avatar1.jpg',
        '/images/avatar2.jpg',
        '/images/avatar3.jpg',
        '/images/avatar4.jpg',
        '/images/avatar5.jpg'
    ];
    return view('pages.contact', ['images' => $images]);
})->name('contact');

Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/process', function () {
    return view('pages.process');
})->name('process');

Route::get('/services', function () {
    return view('pages.service');
})->name('services');

Route::get('/testimonial', function () {
    $images = [
        '/images/avatar1.jpg',
        '/images/avatar2.jpg',
        '/images/avatar3.jpg',
        '/images/avatar4.jpg',
        '/images/avatar5.jpg'
    ];
    return view('pages.testimonial', ['images' => $images]);
})->name('testimonial');

